<?php

$area2 = "";
$title = elgg_echo('moodleoauth:callback:title');
$area2 .= elgg_view_title($title);

// exchange request token with access token
try {
    $oauth_token = $_GET['oauth_token'];
    $oauth_verifier = $_GET['oauth_verifier'];
    $oauth_token_secret = $_GET['oauth_token_secret'];
    $success = false;

    if ($oauth_verifier != '') {
        $session = moodleoauth_get_session();
        if ($session && $session['oauth_token'] != '' && $session['oauth_token_secret'] != '') {
            $oauth = new MoodleOAuth($session['oauth_token'], $session['oauth_token_secret']);
            $token = $oauth->getAccessToken($oauth, $oauth_verifier);
            $success = true;
        } else {
            // no session present
            $area2 .= elgg_echo('moodleoauth:error:login:tokenverify');
        }
    } elseif ($oauth_token_secret != '') {
        $oauth = new MoodleOAuth($oauth_token, $oauth_token_secret);
        $token = $oauth->getToken();
        $success = true;
    }

    if ($success) {
        moodleoauth_save_session($token, true);
        $user = $oauth->getUserInfo();

        // Check that user has valid information
        if ($user && isset($user->id) && isset($user->email) && isset($user->firstname) && isset($user->lastname) && isset($user->username)) {
            // We have retrieved user information from Moodle server
            // Try to login with this user or create a new account as needed
            $moodle_user = new MoodleOAuthUser();

            // If we found an Elgg user linked with this Moodle account, login it
            if ($moodle_user->load_user_by_oauth_user_id($user->id)) {
                $moodle_user->login_user();
                if (elgg_is_logged_in()) {
                    moodleoauth_forward();
                } else {
                    $area2 .= elgg_echo('moodleoauth:error:login') . ".";
                }
            // If we found an Elgg user with the same email as this Moodle account, ask user
            // for its password to verify that its the owner of the two accounts in order to linking them
            } elseif ($moodle_user->load_user_by_email($user->email)) {
                $server_name = moodleoauth_get_parameter('server_name');
                $server_uri = moodleoauth_get_parameter('server_uri');
                $area2 .= "<p>" . elgg_echo('moodleoauth:callback:verifyaccount', array($user->email, $server_uri, $server_name)) . "</p>";

                $form_body .= '<div class="mbm">';
                $form_body .= elgg_echo('email');
                $form_body .= elgg_view('input/text', array('name' => 'email', 'value' => $user->email));
                $form_body .= '</div>';

                $form_body .= '<div class="mbm">';
                $form_body .= elgg_echo('password');
                $form_body .= elgg_view('input/password', array('name' => 'password', 'class' => 'elgg-autofocus',));
                $form_body .= elgg_view('input/hidden', array('name' => 'moodle_userid', 'value' => $user->id));
                $form_body .= '</div>';

                $form_body .= elgg_view('input/submit', array('value' => elgg_echo('moodleoauth:callback:form:button')));

                $area2 .= elgg_view('input/form', array('action' => elgg_get_site_url() . 'action/moodleoauth/linkaccount', 'body' => $form_body));
            // If not, create a new Elgg account with Moodle provided data and login it
            } else {
                $moodle_user->create_user($user);
                if (elgg_is_logged_in()) {
                    moodleoauth_forward();
                } else {
                    $area2 .= elgg_echo('moodleoauth:error:login') . ".";
                }
            }
        } else {
            $area2 .= elgg_echo('moodleoauth:error:login:getuser');
        }
    } else {
        $area2 .= elgg_echo('moodleoauth:error:login:request');
    }
} catch (MoodleOAuthException $e) {
    $area2 .= elgg_echo('moodleoauth:error:login:request') . ':<br /><font color="red">' . $e->getMessage() . '</font>';
}

$body = elgg_view_layout('two_column_left_sidebar', array('area1' => $area1, 'area2' => $area2));

// Draw it
echo elgg_view_page($title, $body);
?>