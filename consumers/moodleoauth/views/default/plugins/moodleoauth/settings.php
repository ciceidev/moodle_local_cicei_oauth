<div>
    <p>
        <?php echo elgg_echo('moodleoauth:settings:info'); ?>
    </p>
    <p>
        <?php echo elgg_echo('moodleoauth:settings:warning'); ?>
    </p>
</div>

<div>
    <label>
        <?php echo elgg_echo('moodleoauth:settings:server_name:label'); ?>
    </label>
    <blockquote>
        <?php echo elgg_echo('moodleoauth:settings:server_name:help'); ?>
    </blockquote>
    <?php
        echo elgg_view('input/text', array(
            'name' => 'params[moodleoauth_server_name]',
            'value' => $vars['entity']->moodleoauth_server_name,
        ));
    ?>
</div>

<div>
    <label>
        <?php echo elgg_echo('moodleoauth:settings:server_uri:label'); ?>
    </label>
    <blockquote>
        <?php echo elgg_echo('moodleoauth:settings:server_uri:help'); ?>
    </blockquote>
    <?php
    echo elgg_view('input/text', array(
        'name' => 'params[moodleoauth_server_uri]',
        'value' => $vars['entity']->moodleoauth_server_uri,
    ));
    ?>
</div>

<div>
    <label>
        <?php echo elgg_echo('moodleoauth:settings:consumer_key:label'); ?>
    </label>
    <blockquote>
        <?php echo elgg_echo('moodleoauth:settings:consumer_key:help'); ?>
    </blockquote>
    <?php
    echo elgg_view('input/text', array(
        'name' => 'params[moodleoauth_consumer_key]',
        'value' => $vars['entity']->moodleoauth_consumer_key,
    ));
    ?>
</div>

<div>
    <label>
        <?php echo elgg_echo('moodleoauth:settings:consumer_secret:label'); ?>
    </label>
    <blockquote>
        <?php echo elgg_echo('moodleoauth:settings:consumer_secret:help'); ?>
    </blockquote>
    <?php
    echo elgg_view('input/text', array(
        'name' => 'params[moodleoauth_consumer_secret]',
        'value' => $vars['entity']->moodleoauth_consumer_secret,
    ));
    ?>
</div>

<div>
    <label>
        <?php echo elgg_echo('moodleoauth:settings:allow_created_unlink:label'); ?>
    </label>
    <blockquote>
        <?php echo elgg_echo('moodleoauth:settings:allow_created_unlink:help'); ?>
    </blockquote>
    <?php
    echo elgg_view('input/dropdown', array(
        'name' => 'params[moodleoauth_allow_created_unlink]',
        'options_values' => array(
            0 => elgg_echo('option:no'),
            1 => elgg_echo('option:yes'),
        ),
        'value' => $vars['entity']->moodleoauth_allow_created_unlink,
    ));
    ?>
</div>

<div>
    <label>
        <?php echo elgg_echo('moodleoauth:settings:icon:label'); ?>
    </label>
    <blockquote>
        <?php echo elgg_echo('moodleoauth:settings:icon:help'); ?>
    </blockquote>
    <?php
    echo elgg_view('input/text', array(
        'name' => 'params[moodleoauth_icon]',
        'value' => $vars['entity']->moodleoauth_icon,
    ));
    ?>
</div>