<?php

$english = array(
    // General
    'moodleoauth:callback:title' => "Login authorization",
    'moodleoauth:callback:form:button' => "Check",
    'moodleoauth:callback:verifyaccount' => "The email address <b>%s</b> fetched from <a href=\"%s\">%s</a> is already present in this system. Please write your password to verify that you are the owner of this account in order to link them. If you aren't the owner of that account or if you want to link another account, just write below your credentials.",
    'moodleoauth:login:info' => "You can also login using your account from this sites:",
    'moodleoauth:login:title' => "Requesting login token",
    // Plugin settings
    'moodleoauth:settings:allow_created_unlink:help' => "If you select this, you will allow users created with Moodle accounts data to unlink their accounts requesting a new password.",
    'moodleoauth:settings:allow_created_unlink:label' => "Allow created accounts unlinking?",
    'moodleoauth:settings:consumer_key:help' => "Consumer key to sign OAuth requests.",
    'moodleoauth:settings:consumer_key:label' => "Consumer Key",
    'moodleoauth:settings:consumer_secret:help' => "Secret key to sign OAuth requests.",
    'moodleoauth:settings:consumer_secret:label' => "Consumer Secret",
    'moodleoauth:settings:icon:help' => "Url to image to show in login windows instead of text. If empty, default image located in mod/moodleoauth/img/icon.png will be used.",
    'moodleoauth:settings:icon:label' => "Image URL",
    'moodleoauth:settings:info' => "You must fill all this fields to be able to login through your Moodle server.",
    'moodleoauth:settings:server_name:help' => "Name to indentify the server. Users will see this name through the site.",
    'moodleoauth:settings:server_name:label' => "Server Name",
    'moodleoauth:settings:server_uri:help' => "Base url of the server without the ending slash /.",
    'moodleoauth:settings:server_uri:label' => "Server Base URL",
    'moodleoauth:settings:warning' => "<b>WARNING</b>: after setting up a Moodle server, if users use the plugin to create accounts and login, you should not change to another Moodle server.",
    // Plugin user settings
    'moodleoauth:usersettings:link' => "Click here to link your account with your <b>%s</b> account. You could be asked to enter your credentials from %s and authorize access to your account.",
    'moodleoauth:usersettings:linked:info' => "Your account is linked with your account in <b>%s</b>.",
    'moodleoauth:usersettings:requestnewpassword:info' => "You can obtain a new password for this account using the 'forgot my password function' in this link:",
    'moodleoauth:usersettings:requestnewpassword:link' => "Click here to request a new password.",
    'moodleoauth:usersettings:requestnewpassword:confirm' => "Do you want to request a new password? An email will be sent to your email with a link to create a new password.",
    'moodleoauth:usersettings:unlink' => "Click here to unlink your account with your account in <b>%s</b>.",
    'moodleoauth:usersettings:unlink:disabled' => "You can't unlink your account because was created without a password and you authenticate with your account from <b>%s</b>.",
    'moodleoauth:usersettings:unlink:confirm' => "Do you want to unlink your account? You can link them again in the future.",
    'moodleoauth:usersettings:unlinked:info' => "If you have an account in <b>%s</b> you can link it with your account in this site, so you will be able to log in this site automatically if you are already logged in <b>%s</b>.",
    // Error messages
    'moodleoauth:error:accountnotlinked' => "Unable to unlink your account because is not linked",
    'moodleoauth:error:email' => "Cannot found an account with provided email",
    'moodleoauth:error:input' => "Incorrect input data",
    'moodleoauth:error:linkaccount' => "Unable to link your account in this site with your account in Moodle",
    'moodleoauth:error:password' => "Provided username & password don't match",
    'moodleoauth:error:login' => "The system encountered a problem while logging you in",
    'moodleoauth:error:login:request' => "Error encountered when trying to fetch token",
    'moodleoauth:error:login:tokenverify' => "Error verifying login token",
    'moodleoauth:error:unlinkaccount' => "Unable to unlink your account in this site with your account in Moodle",
    'moodleoauth:error:login:getuser' => "Error encountered when trying to fetch your Moodle user information",
    // Success messages
    'moodleoauth:sucess:linkaccount' => "Your account in this site is now linked with your account in Moodle",
    'moodleoauth:sucess:unlinkaccount' => "You have unlinked your account in this site with your account in Moodle",
);

add_translation("en", $english);
