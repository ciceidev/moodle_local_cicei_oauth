<?php

function moodleoauth_get_parameter($name) {
    switch ($name) {
        case 'oauth_consumer_key':
            $parameter_value = elgg_get_plugin_setting('moodleoauth_consumer_key', 'moodleoauth');
            break;
        case 'oauth_consumer_secret':
            $parameter_value = elgg_get_plugin_setting('moodleoauth_consumer_secret', 'moodleoauth');
            break;
        case 'server_uri':
            $parameter_value = elgg_get_plugin_setting('moodleoauth_server_uri', 'moodleoauth');
            break;
        case 'server_name':
            $parameter_value = elgg_get_plugin_setting('moodleoauth_server_name', 'moodleoauth');
            break;
        default:
            $parameter_value = '';
    }
    return $parameter_value;
}

function moodleoauth_save_session($token, $access_token = false) {
    if ($token['oauth_token'] != '' && $token['oauth_token_secret']) {
        if ($access_token) {
            unset($_SESSION['moodleoauth']['oauth_token']);
            unset($_SESSION['moodleoauth']['oauth_token_secret']);
            $_SESSION['moodleoauth']['oauth_token_access'] = $token['oauth_token'];
            $_SESSION['moodleoauth']['oauth_token_secret_access'] = $token['oauth_token_secret'];
        } else {
            unset($_SESSION['moodleoauth']['oauth_token_access']);
            unset($_SESSION['moodleoauth']['oauth_token_secret_access']);
            $_SESSION['moodleoauth']['oauth_token'] = $token['oauth_token'];
            $_SESSION['moodleoauth']['oauth_token_secret'] = $token['oauth_token_secret'];
        }
    } else {
        // clear token?
        unset($_SESSION['moodleoauth']);
    }
}

function moodleoauth_get_session() {
    if (isset($_SESSION['moodleoauth'])) {
        return $_SESSION['moodleoauth'];
    } else {
        return false;
    }
}

function moodleoauth_forward() {
    // set forward url
    if (isset($_SESSION['last_forward_from']) && $_SESSION['last_forward_from']) {
        $forward_url = $_SESSION['last_forward_from'];
        unset($_SESSION['last_forward_from']);
    } elseif (get_input('returntoreferer')) {
        $forward_url = REFERER;
    } else {
        // forward to main index page
        $forward_url = '';
    }
    forward($forward_url);
}
?>