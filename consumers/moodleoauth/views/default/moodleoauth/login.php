<?php

$login_url = elgg_get_site_url() . 'moodleoauth/login';

$is_configured = (moodleoauth_get_parameter('server_name') != '' &&
                  moodleoauth_get_parameter('oauth_consumer_key') != '' &&
                  moodleoauth_get_parameter('oauth_consumer_secret') != '' &&
                  moodleoauth_get_parameter('server_uri') != '');

if (!$is_configured) {
    return;
}

$server_name = moodleoauth_get_parameter('server_name');
$moodleoauth_icon = elgg_get_plugin_setting('moodleoauth_icon', 'moodleoauth');
if ($moodleoauth_icon) {
    $icon = $moodleoauth_icon;
} else {
    $icon = elgg_get_site_url() . 'mod/moodleoauth/img/icon.png';
}

?>
<div class="elgg-list">
    <p>
        <?php echo elgg_echo('moodleoauth:login:info'); ?>
    </p>
    <ul>
        <li>
            <div class="elgg-photo float" style="width: 100px; text-align: center; padding: 2px; margin-left: 4px;">
                <a href="<?php echo $login_url; ?>" title="<?php echo $server_name; ?>">
                    <img src="<?php echo $icon; ?>" width="100px">
                    <br />
                    <?php echo $server_name; ?>
                </a>
            </div>
        </li>
    </ul>
</div>