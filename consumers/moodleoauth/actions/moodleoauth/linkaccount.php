<?php

/**
 * Link Elgg account with Moodle account if password its correct
 */

// User information from Moodle server
$email = get_input('email');
$moodle_userid = get_input('moodle_userid');

// Password of Elgg user account
$password = get_input('password');

// Check that we have all input values
if (!$email || !$moodle_userid || !$password) {
    register_error(elgg_echo('moodleoauth:error:input'));
    forward(REFERRER);
}

// Check for user account
if (elgg_is_logged_in()) {
    // Load logged user
    $moodle_user = new MoodleOAuthUser(elgg_get_logged_in_user_entity());
} else {
    // Try to found an user with provided email
    $moodle_user = new MoodleOAuthUser();
    if (!$moodle_user->load_user_by_email($email)) {
        register_error(elgg_echo('moodleoauth:error:email'));
        forward(REFERRER);
    }
}
$user = $moodle_user->user;

// Check password
if ($user->password !== generate_user_password($user, $password)) {
    register_error(elgg_echo('moodleoauth:error:password'));
    forward(REFERRER);
}

// Link Elgg account with oauth account
if ($moodle_user->link_user($moodle_userid)) {
    $moodle_user->login_user();
    system_message(elgg_echo('moodleoauth:sucess:linkaccount'));
} else {
    register_error(elgg_echo('moodleoauth:error:linkaccount'));
}

forward(REFERRER);
