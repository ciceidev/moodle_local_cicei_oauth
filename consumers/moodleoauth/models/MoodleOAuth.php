<?php

define('MOODLE_OAUTH_CONSUMER_KEY', moodleoauth_get_parameter('oauth_consumer_key'));
define('MOODLE_OAUTH_CONSUMER_SECRET', moodleoauth_get_parameter('oauth_consumer_secret'));
define('MOODLE_OAUTH_SERVER_URI', moodleoauth_get_parameter('server_uri'));

class MoodleOAuth {

    private $http_status;
    private $last_api_call;
    public static $TO_API_ROOT = MOODLE_OAUTH_SERVER_URI;

    function requestTokenURL() {
        return self::$TO_API_ROOT . '/local/cicei_oauth/request.php';
    }

    function authorizeURL() {
        return self::$TO_API_ROOT . '/local/cicei_oauth/authorize.php';
    }

    function accessTokenURL() {
        return self::$TO_API_ROOT . '/local/cicei_oauth/access.php';
    }

    function restURL() {
        return self::$TO_API_ROOT . '/local/cicei_oauth/rest.php';
    }

    function lastStatusCode() {
        return $this->http_status;
    }

    function lastAPICall() {
        return $this->last_api_call;
    }

    function __construct($oauth_token = NULL, $oauth_token_secret = NULL) {
        $consumer_key = MOODLE_OAUTH_CONSUMER_KEY;
        $consumer_secret = MOODLE_OAUTH_CONSUMER_SECRET;
        $this->sha1_method = new MoodleOAuthSignatureMethod_HMAC_SHA1();
        $this->consumer = new MoodleOAuthConsumer($consumer_key, $consumer_secret);

        if (!empty($oauth_token) && !empty($oauth_token_secret)) {
            $this->token = new MoodleOAuthToken($oauth_token, $oauth_token_secret);
        } else {
            $this->token = NULL;
        }
    }

    function getRequestToken($args = array()) {
        $r = $this->oAuthRequest($this->requestTokenURL(), $args);
        $token = $this->oAuthParseResponse($r);
        if (is_array($token) && array_key_exists('oauth_token', $token)) {
            $this->token = new MoodleOAuthToken($token['oauth_token'], $token['oauth_token_secret']);
            return $token;
        } else {
            throw new MoodleOAuthException($r);
        }
    }

    function oAuthParseResponse($responseString) {
        $r = array();
        foreach (explode('&', $responseString) as $param) {
            $pair = explode('=', $param, 2);
            if (count($pair) != 2)
                continue;
            $r[urldecode($pair[0])] = urldecode($pair[1]);
        }
        return $r;
    }

    function getAuthorizeURL($token) {
        if (is_array($token))
            $token = $token['oauth_token'];
        return $this->authorizeURL() . '?oauth_token=' . $token;
    }

    function getAccessToken($token = NULL, $pin = NULL) {
        if ($pin)
            $r = $this->oAuthRequest(
                    $this->accessTokenURL(), array("oauth_verifier" => $pin));
        else
            $r = $this->oAuthRequest($this->accessTokenURL());

        $token = $this->oAuthParseResponse($r);
        if (is_array($token) && array_key_exists('oauth_token', $token)) {
            $this->token = new MoodleOAuthToken($token['oauth_token'], $token['oauth_token_secret']);
            return $token;
        } else {
            throw new MoodleOAuthException($r);
        }
    }

    function getToken() {
        if ($this->token && $this->token->key != '' && $this->token->secret != '') {
            $token = array('oauth_token' => $this->token->key, 'oauth_token_secret' => $this->token->secret);
            return $token;
        } else {
            return NULL;
        }
    }

    function getUserInfo() {
        $token = $this->getToken();
        if ($token) {
            $args = array('oauth_consumer_key' => $this->consumer->key,
                'oauth_token' => $token['oauth_token'],
                'rest_service' => 'user.info');
            $r = $this->oAuthRequest($this->restURL(), $args, "POST");
            return json_decode($r);
        } else {
            return NULL;
        }
    }

    function oAuthRequest($url, $args = array(), $method = NULL) {/* {{{ */
        if (empty($method))
            $method = empty($args) ? "GET" : "POST";
        $req = MoodleOAuthRequest::from_consumer_and_token($this->consumer, $this->token, $method, $url, $args);
        $req->sign_request($this->sha1_method, $this->consumer, $this->token);
        switch ($method) {
            case 'GET': return $this->http($req->to_url());
            case 'POST': return $this->http($req->get_normalized_http_url(), $req->to_postdata());
        }
    }

    function http($url, $post_data = null) {/* {{{ */
        $ch = curl_init();
        if (defined("CURL_CA_BUNDLE_PATH"))
            curl_setopt($ch, CURLOPT_CAINFO, CURL_CA_BUNDLE_PATH);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        //////////////////////////////////////////////////
        ///// Set to 1 to verify Twitter's SSL Cert //////
        //////////////////////////////////////////////////
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if (isset($post_data)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        }
        $response = curl_exec($ch);
        $this->http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->last_api_call = $url;
        curl_close($ch);
        return $response;
    }

}

Class MoodleOAuthUser {
    // Stores associated Elgg user
    public $user;

    function __construct($user = null) {
        $this->user = $user;
    }

        /**
     * Creates a new Elgg user using data form Moodle
     * @param type $moodle_user
     * @return boolean
     */
    function create_user($moodle_user) {
        // Validate email
        try {
            validate_email_address($moodle_user->email);
        } catch (RegistrationException $ex) {
            register_error(elgg_echo('registration:emailnotvalid'));
            return false;
        }

        // Validate username
        try {
            validate_username($moodle_user->username);
        } catch (RegistrationException $ex) {
            register_error(elgg_echo('registration:usernamenotvalid') . ' ' . $ex->getMessage());
            return false;
        }

        // check if email is alredy in the system
        if (get_user_by_email($moodle_user->email)) {
            return false;
        }

        // check if username is already taken and generate one as needed
        if (get_user_by_username($moodle_user->username)) {
            $append_ctr = 0;
            do {
                $append_ctr++;
                $moodle_user->username = $moodle_user->username . '-' . $append_ctr;
            } while (get_user_by_username($moodle_user->username));
        }

        // Create a new user account
        $newuser = new ElggUser();
        $newuser->email = $moodle_user->email;
        $newuser->access_id = ACCESS_PUBLIC;
        $newuser->username = $moodle_user->username;
        $newuser->name = $moodle_user->firstname . $moodle_user->lastname;
        $newuser->salt = generate_random_cleartext_password(); // Note salt generated before password!
        $newuser->password = generate_user_password($newuser, generate_random_cleartext_password());
        $newuser->owner_guid = 0; // Users aren't owned by anyone, even if they are admin created.
        $newuser->container_guid = 0; // Users aren't contained by anyone, even if they are admin created.

        // Save and login the new user
        if ($newuser->save()) {
            $this->user = $newuser;

            // Validate user
            elgg_set_user_validation_status($newuser->getGUID(), TRUE, 'moodleoauth');

            // Turn on email notifications by default
            set_user_notification_setting($newuser->getGUID(), 'email', true);

            // Link this account to moodle user id
            $this->link_user($moodle_user->id);

            // Add user picture to account if provided
            if ($moodle_user->picture) {
                // User picture assign code getted from actions/avatar/upload.php
                $errors = false;

                // Get icon sizes
                $icon_sizes = elgg_get_config('icon_sizes');

                // get the images and save their file handlers into an array
                // so we can do clean up if one fails.
                $files = array();
                foreach ($icon_sizes as $name => $size_info) {
                    $resized = get_resized_image_from_existing_file($moodle_user->picture, $size_info['w'], $size_info['h'], $size_info['square']);

                    if ($resized) {
                        $file = new ElggFile();
                        $file->owner_guid = $newuser->guid;
                        $file->setFilename("profile/{$newuser->guid}{$name}.jpg");
                        $file->open('write');
                        $file->write($resized);
                        $file->close();
                        $files[] = $file;
                    } else {
                        // cleanup on fail
                        foreach ($files as $file) {
                            $file->delete();
                        }
                        $errors = true;
                    }
                }

                if (!$errors) {
                    // reset crop coordinates
                    $newuser->x1 = 0;
                    $newuser->x2 = 0;
                    $newuser->y1 = 0;
                    $newuser->y2 = 0;

                    $newuser->icontime = time();
                }
            }

            // Mark this account as created by this plugin using Moodle data
            // This is to prevent account unlinking because the acount doesn't have a valid password
            $this->set_moodle_oauth_created_user();

            // Login this user into Elgg
            return $this->login_user();
        }

        return false;
    }

    /**
     * Try to load Elgg user by email
     * @param type $email
     * @return boolean
     */
    function load_user_by_email($email = false) {
        if (!$email || $email == '') {
            return false;
        } else {
            $users = get_user_by_email($email);
            if ($users) {
                $this->user = current($users);

                // if user is banned, ignore it
                if ($this->user->isBanned()) {
                    return false;
                } else {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Try to load Elgg user using Moodle user id
     * @param type $moodle_userid
     * @return boolean
     */
    function load_user_by_oauth_user_id($moodle_userid = false) {
        if (!$moodle_userid) {
            return false;
        }

        $options = array(
            'plugin_id' => 'moodleoauth',
            'types' => 'user',
            'plugin_user_setting_name_value_pairs' => array('moodle_userid' => $moodle_userid),
            'limit' => 1);
        $users = elgg_get_entities_from_plugin_user_settings($options);

        if ($users) {
            $this->user = current($users);

            // if user is banned, ignore it
            if ($this->user->isBanned()) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * Login this user into Elgg
     * @return boolean
     */
    function login_user() {
        if ($this->user) {
            login($this->user);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if this user account is linked to a Moodle user id
     * @return boolean
     */
    function is_linked() {
        if ($this->user) {
            return elgg_get_plugin_user_setting('moodle_userid', $this->user->getGUID(), 'moodleoauth');
        }
        return false;
    }

    /**
     * Link this user account to Moodle user id
     * @param type $moodle_userid
     * @return boolean
     */
    function link_user($moodle_userid) {
        if (!$this->is_linked() && $moodle_userid) {
            return elgg_set_plugin_user_setting('moodle_userid', $moodle_userid, $this->user->getGUID(), 'moodleoauth');
        }
        return false;
    }

    /**
     * Unlink this user account from assigned Moodle user id
     * @return boolean
     */
    function unlink_user() {
        if ($this->is_linked()) {
            return elgg_unset_plugin_user_setting('moodle_userid', $this->user->getGUID(), 'moodleoauth');
        }
        return false;
    }

    /**
     * Check if this user account was created from this plugin.
     * These accounts don't have a valid password.
     * @return boolean
     */
    function is_moodle_oauth_created_user() {
        if ($this->user) {
            return elgg_get_plugin_user_setting('moodleoauth_created_user', $this->user->getGUID(), 'moodleoauth');
        }
        return false;
    }

    /**
     * Mark this user as created by this plugin
     * @return boolean
     */
    function set_moodle_oauth_created_user() {
        if ($this->user) {
            return elgg_set_plugin_user_setting('moodleoauth_created_user', true, $this->user->getGUID(), 'moodleoauth');
        }
        return false;
    }

    /**
     * UnMark this user as created by this plugin
     * @return boolean
     */
    function unset_moodle_oauth_created_user() {
        if ($this->user) {
            return elgg_unset_plugin_user_setting('moodleoauth_created_user', $this->user->getGUID(), 'moodleoauth');
        }
        return false;
    }
}

?>
