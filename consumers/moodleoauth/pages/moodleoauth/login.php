<?php

$area2 = "";
$title = elgg_echo('moodleoauth:login:title');
$area2 .= elgg_view_title($title);

try {
    $oauth = new MoodleOAuth();
    $args = array();
    $args['oauth_callback'] = elgg_get_site_url() . 'moodleoauth/callback/';
    $token = $oauth->getRequestToken($args);
    moodleoauth_save_session($token);

    forward($oauth->getAuthorizeURL($token['oauth_token']));
} catch (MoodleOAuthException $e) {
    $area2 .= elgg_echo('moodleoauth:error:login:request') . ':<br /><font color="red">' . $e->getMessage() . '</font>';
}

$body = elgg_view_layout('two_column_left_sidebar', array('area1' => $area1, 'area2' => $area2));

// Draw it
echo elgg_view_page($title, $body);
?>