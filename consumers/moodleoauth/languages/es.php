<?php

$spanish = array(
    // General
    'moodleoauth:callback:title' => "Autorizando inicio de sesión",
    'moodleoauth:callback:form:button' => "Comprobar",
    'moodleoauth:callback:verifyaccount' => "La dirección de correo <b>%s</b>, obtenida de <a href=\"%s\">%s</a> ya se encuentra registrada en este sistema. Por favor, escribe tu contraseña para verificar que eres el dueño de la cuenta para enlazarla. Si no eres el propietario de esa cuenta o si quieres enlazarla con otra, escribe debajo tus credenciales.",
    'moodleoauth:login:info' => "También puedes iniciar sesión con tu cuenta de:",
    'moodleoauth:login:title' => "Solicitando token de inicion de sesión",
    // Plugin settings
    'moodleoauth:settings:allow_created_unlink:help' => "Si seleccionas esta opción, permitiras a los usuarios creados con los datos de sus cuentas de Moodle desenlazar sus cuentas solicitando una contraseña nueva.",
    'moodleoauth:settings:allow_created_unlink:label' => "¿Permitir a las cuentas creadas desenlazarse?",
    'moodleoauth:settings:consumer_key:help' => "Consumer key para firmar las peticiones OAuth.",
    'moodleoauth:settings:consumer_key:label' => "Consumer Key",
    'moodleoauth:settings:consumer_secret:help' => "Secret key para firmar las peticiones OAuth.",
    'moodleoauth:settings:consumer_secret:label' => "Consumer Secret",
    'moodleoauth:settings:icon:help' => "Url a una imagen para mostrar en las ventanas de inicio de sesión. Si está vacío, se utiliza la imagen por defecto localizada en mod/moodleoauth/img/icon.png.",
    'moodleoauth:settings:icon:label' => "URL a Imagen",
    'moodleoauth:settings:info' => "Debes rellenar todos los campos para poder iniciar sesión por medio de tu servidor de Moodle.",
    'moodleoauth:settings:server_name:help' => "Nombre para identificar el servidor. Los usuarios verán este nombre por el sitio.",
    'moodleoauth:settings:server_name:label' => "Nombre del Servidor",
    'moodleoauth:settings:server_uri:help' => "URL base del servidor sin la barra del final /.",
    'moodleoauth:settings:server_uri:label' => "URL Base del Servidor",
    'moodleoauth:settings:warning' => "<b>ATENCIÓN</b>: después de configurar un servidor de Moodle, si los usuarios utilizan el plugin para crear cuentas e iniciar sesión, no deberías cambiar a otro servidor de Moodle.",
    // Plugin user settings
    'moodleoauth:usersettings:link' => "Haz clic aquí para enlazar tu cuenta en este sitio con tu cuenta en <b>%s</b>. Puede ser necesario que introduzcas tus credenciales de %s y que autorices el acceso a los datos de tu cuenta.",
    'moodleoauth:usersettings:linked:info' => "Tu cuenta está enlazada con tu cuenta en <b>%s</b>.",
    'moodleoauth:usersettings:requestnewpassword:info' => "Puede sobtener una contraseña nueva para tu cuenta utilizando la función 'olvidé mi contraseña' en este enlace:",
    'moodleoauth:usersettings:requestnewpassword:link' => "Haz clic aquí para solicitar una contraseña nueva.",
    'moodleoauth:usersettings:requestnewpassword:confirm' => "¿Quieres solicitar una contraseña nueva? Se enviará un email a tu dirección de correo con un enlace para crear una nueva contraseña.",
    'moodleoauth:usersettings:unlink' => "Haz clic aquí para desenlazar tu cuenta con tu cuenta en <b>%s</b>.",
    'moodleoauth:usersettings:unlink:disabled' => "No puedes desenlazar tu cuenta porque fue creada sin una contraseña y tu inicias sesión con tu cuenta de <b>%s</b>.",
    'moodleoauth:usersettings:unlink:confirm' => "¿Quieres desenlazar tu cuenta? Puedes enlazarlas otra vez en el futuro.",
    'moodleoauth:usersettings:unlinked:info' => "Si tu tienes una cuenta en <b>%s</b> puedes enlazarla con tu cuenta en este sitio, así podras iniciar sesión en este sitio automáticamente si ya tienes la sesión iniciada en <b>%s</b>.",
    // Error messages
    'moodleoauth:error:accountnotlinked' => "No se pudo desenlazar tu cuenta porque no está enlazada",
    'moodleoauth:error:email' => "No se puedo encontrar una cuenta con el email proporcionado",
    'moodleoauth:error:input' => "Datos de entrada incorrectos",
    'moodleoauth:error:linkaccount' => "No se pudo enlazar tu cuenta en este sitio con tu cuenta en Moodle",
    'moodleoauth:error:password' => "El usuario y la contraseña proporcionado no coinciden",
    'moodleoauth:error:login' => "El sistema tuvo un problema mientras iniciaba sesión",
    'moodleoauth:error:login:request' => "Error cuando se intentaba obtener un token",
    'moodleoauth:error:login:tokenverify' => "Error verificando token de inicio de sesión",
    'moodleoauth:error:unlinkaccount' => "No se pudo desenlazar tu cuenta en este sitio con tu cuenta en Moodle",
    'moodleoauth:error:login:getuser' => "Error cuando se intentaba obtener la información del usuario",
    // Success messages
    'moodleoauth:sucess:linkaccount' => "Tu cuenta en este sitio se ha enlazado con tu cuenta en Moodle",
    'moodleoauth:sucess:unlinkaccount' => "Has desenlazado tu cuenta en este sitio con tu cuenta en Moodle",
);

add_translation("es", $spanish);
