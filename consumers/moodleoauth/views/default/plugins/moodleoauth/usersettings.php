<?php
/**
 * User settings for Twitter API
 */

$user = elgg_get_logged_in_user_entity();
$oauth_user = new MoodleOAuthUser($user);

$server_name = moodleoauth_get_parameter('server_name');

if ($oauth_user->is_linked()) {
    // Show unlink account options
    $content = "";
    $content .= "<p>" . elgg_echo('moodleoauth:usersettings:linked:info', array($server_name)) . "</p>";
    if (!$oauth_user->is_moodle_oauth_created_user()) {
        $params = array(
            'text' => elgg_echo('moodleoauth:usersettings:unlink', array($server_name)),
            'href' => elgg_get_site_url() . 'action/moodleoauth/unlinkaccount',
            'confirm' => elgg_echo('moodleoauth:usersettings:unlink:confirm'));
        $content .= elgg_view('output/confirmlink', $params);
    } else {
        $content .= "<p>" . elgg_echo('moodleoauth:usersettings:unlink:disabled', array($server_name)) . "</p>";
        if (elgg_get_plugin_setting('moodleoauth_allow_created_unlink', 'moodleoauth')) {
            $content .= "<p>" . elgg_echo('moodleoauth:usersettings:requestnewpassword:info') . "</p>";
            $params = array(
                'text' => elgg_echo('moodleoauth:usersettings:requestnewpassword:link'),
                'href' => elgg_add_action_tokens_to_url(elgg_get_site_url() . "action/moodleoauth/requestnewpassword?username=$user->username"),
                'confirm' => elgg_echo('moodleoauth:usersettings:requestnewpassword:confirm'));
            $content .= elgg_view('output/confirmlink', $params);
        }
    }
} else {
    // Show link account option
    $content = "<p>" . elgg_echo('moodleoauth:usersettings:unlinked:info', array($server_name, $server_name)) . "</p>";
    $params = array(
        'text' => elgg_echo('moodleoauth:usersettings:link', array($server_name, $server_name)),
        'href' => elgg_get_site_url() . 'moodleoauth/login');
    $content .= elgg_view('output/url', $params);
}

echo "<div>$content</div>";