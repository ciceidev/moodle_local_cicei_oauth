<?php

require_once('lib/lib.php');
require_once('models/OAuth.php');
require_once('models/MoodleOAuth.php');

function moodleoauth_init() {
    elgg_register_action('moodleoauth/linkaccount', elgg_get_plugins_path() . 'moodleoauth/actions/moodleoauth/linkaccount.php', 'public');
    elgg_register_action('moodleoauth/unlinkaccount', elgg_get_plugins_path() . 'moodleoauth/actions/moodleoauth/unlinkaccount.php');
    if (elgg_get_plugin_setting('moodleoauth_allow_created_unlink', 'moodleoauth')) {
        elgg_register_action('moodleoauth/requestnewpassword', elgg_get_plugins_path() . 'moodleoauth/actions/moodleoauth/requestnewpassword.php');
    }
    elgg_extend_view('forms/login', 'moodleoauth/login');
    elgg_register_page_handler('moodleoauth', 'moodleoauth_page_handler');
}

function moodleoauth_page_handler($page) {
    global $CONFIG;

    $base_dir = elgg_get_plugins_path() . 'moodleoauth/pages/moodleoauth/';

    switch ($page[0]) {
        case 'login':
        case 'callback':
            include($base_dir . $page[0] . '.php');
    }
}

elgg_register_event_handler('init', 'system', 'moodleoauth_init');
?>