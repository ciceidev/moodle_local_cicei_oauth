<?php
/**
 * Action to request a new password.
 * Based in Elgg core action.
 */

$username = get_input('username');

// allow email addresses
if (strpos($username, '@') !== false && ($users = get_user_by_email($username))) {
	$username = $users[0]->username;
}

$user = get_user_by_username($username);
if ($user) {
	if (send_new_password_request($user->guid)) {
        // always set Moodle created user as not created from this plugin, so user can unlink their account
        $oauth_user = new MoodleOAuthUser($user);
        $oauth_user->unset_moodle_oauth_created_user();
		system_message(elgg_echo('user:password:resetreq:success'));
	} else {
		register_error(elgg_echo('user:password:resetreq:fail'));
	}
} else {
	register_error(elgg_echo('user:username:notfound', array($username)));
}

forward();
