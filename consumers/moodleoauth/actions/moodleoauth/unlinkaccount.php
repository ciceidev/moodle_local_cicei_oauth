<?php

/**
 * Unlink Elgg account from Moodle account
 */

// Get user information from Moodle server (action requires that user is logged in)
$user = elgg_get_logged_in_user_entity();
$oauth_user = new MoodleOAuthUser($user);

// Unlink Elgg account with Moodle account if it wasn't created by this plugin
if ($oauth_user->is_linked()) {
    if (!$oauth_user->is_moodle_oauth_created_user()) {
        if ($oauth_user->unlink_user()) {
            system_message(elgg_echo('moodleoauth:sucess:unlinkaccount'));
        } else {
            register_error(elgg_echo('moodleoauth:error:unlinkaccount'));
        }
    } else {
        register_error(elgg_echo('moodleoauth:error:unlinkaccount'));
    }
    forward(REFERRER);
} else {
    register_error(elgg_echo('moodleoauth:error:accountnotlinked'));
    forward(REFERRER);
}
